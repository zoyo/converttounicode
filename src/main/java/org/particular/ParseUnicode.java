package org.particular;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JOptionPane;

public class ParseUnicode {
	public static void main(String[] args) {
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		
		if(args != null && args.length > 1) {
			
			if("-h".equalsIgnoreCase(args[1])) {
				System.out.println("Uso: chame o programa informando a mensagem que deseja transformar");
				return;
			}
			
			StringBuilder str = new StringBuilder();
			
			for (String s : args) {
				String strToShow = String.format("%s : %s", s, parseToUnicode(s));
				System.out.println(strToShow);
				str.append(strToShow).append("\n");
			}
			System.out.println("Texto em unicode enviado para área de transferência");
			clipboard.setContents(new StringSelection(str.toString()), null);
			
		} else {
			String textoParaConversao = ""; 
			do {
				textoParaConversao = 
						JOptionPane.showInputDialog(null, "Informe o texto para conversão", "Converter para Unicode", JOptionPane.QUESTION_MESSAGE);
				
				if(textoParaConversao != null && !textoParaConversao.trim().isEmpty()) {
					JOptionPane.showMessageDialog(null, 
										String.format("Orginal: %s\nUnicode: %s\n\nClique em OK para enviar para área de transferência", 
												textoParaConversao, 
												parseToUnicode(textoParaConversao)), 
										"Resultado", 
										JOptionPane.INFORMATION_MESSAGE);
					clipboard.setContents(new StringSelection(parseToUnicode(textoParaConversao)), null);
				
				}
			} while(textoParaConversao != null && !textoParaConversao.trim().isEmpty());
		}
	}
	
	public static String parseToUnicode(String s) {
		Pattern pattern = Pattern.compile("([áéíóúÁÉÍÓÚâêîôûÂÊÎÔÛàèìòùÀÈÌÒÙãÃñÑçÇ])");
		Matcher matcher = pattern.matcher(s);
		
		StringBuffer sb = new StringBuffer();
		while(matcher.find()) {
			String charToEncoded = matcher.group(1);
			matcher.appendReplacement(sb, String.format("\\\\u%04X",(int) charToEncoded.toCharArray()[0]));
		}
		matcher.appendTail(sb);
		
		return sb.toString();
	}
}
